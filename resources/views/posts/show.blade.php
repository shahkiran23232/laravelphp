@extends('layouts.app')

@section('content')
<div class="container-sm bg-secondary text-white"><br/>
    <a href="/posts" class="btn btn-success">Go Back</a>
    <div class="justify-content-center">
        <h1>{{ $post->title}}</h1>
        <div>
        <img style="width:100%" src="/storage/cover_images/{{$post->cover_images}}">
        </div>
        <div class="info">
            {{ $post->content}}
        </div>
        <p> posted on {{$post->created_at}}</p>
    </div>
    <div>
    <a href="/posts/{{$post->id}}/edit" class="btn btn-info btn-lg btn-block">Edit</a>
    <br/>
    {!! Form::open(['action'=>['PostController@destroy', $post->id], 'method'=> 'POST']) !!}
        {{Form::hidden('_method','DELETE')}}
        {{ Form::submit('Delete', ['class'=>'btn btn-danger btn-lg btn-block']) }}
    {!! Form::close() !!}
    </div>

</div>
@endsection
