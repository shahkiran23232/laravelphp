@extends('layouts.app')

@section('content')
    @include ('includes.message-block')
    <div class="container-sm bg-secondary text-white">
    <h1>Edit Post</h1>
    {!! Form::open(['action' => ['PostController@update', $post->id], 'method' => 'Post', 'enctype'=>'multipart/form-data'])!!}
        <div class="Form-group  mb-2">
            {{ Form::label('title', 'Title')}}
            {{ Form::text('title', $post->title,['class'=> 'form-control', 'placeholder'=>'Title'])}}
        </div>
        <div class="Form-group  mb-2">
            {{ Form::label('content', 'Content')}}
            {{ Form::textarea('content', $post->content,['class'=> 'form-control', 'placeholder'=>'Content'])}}
        </div>
        <div class="Form-group mb-2">
            {{ Form::file('cover_image')}}
        </div>
            {{ Form::submit('Submit', ['class'=>'btn btn-success']) }}
        {{Form::hidden('_method','PUT')}}
    {!! Form::close() !!}
    </div>
@endsection
